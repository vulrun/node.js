const jwt = require("jsonwebtoken");
const Model = require("../models");

module.exports.getToken = (data) => jwt.sign(data, process.env.SECRET_KEY, { expiresIn: "99 years" });

module.exports.verifyToken = (token) => jwt.verify(token, process.env.SECRET_KEY);

module.exports.verify = (role) => async (req, res, next) => {
    try {
        const token = String(req.headers.authorization || "")
            .replace(/bearer|jwt/i, "")
            .replace(/^\s+|\s+$/g, "");

        const decoded = await this.verifyToken(token);
        const userObj = await Model.Users.findOne({ _id: decoded._id, isDeleted: false }).lean();
        if (!userObj) throw new Error("INVALID_TOKEN");

        const session = await Model.Sessions.findOne({ userId: userObj._id, accessToken: token, isDeleted: false }).lean();
        if (!session) throw new Error("INVALID_SESSION");

        req.user = { ...userObj, session };
        next();
    } catch (error) {
        console.error(error);
        const message = String(error.name).toLowerCase() === "error" ? error.message : "UNAUTHORIZED_ACCESS";
        res.error(message, 401);
    }
};
