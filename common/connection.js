const mongoose = require("mongoose");
global.ObjectId = mongoose.Types.ObjectId;

module.exports.mongodb = () => {
    // Connecting to Database
    mongoose
        .connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })
        .then(
            (db) => console.log("MongoDB", "Connected"),
            (err) => console.log("MongoDB", err.message)
        );
};
