const { nanoid, customAlphabet } = require("nanoid");

module.exports.generateString = (length) => nanoid(length);
module.exports.generateNumber = (length) => customAlphabet("1234567890", length)();
module.exports.generateCustom = (length, charset) => customAlphabet(charset, length)();

module.exports.toDecimals = (val, decimal = 2) => {
    const base = Math.pow(10, decimal);
    return Math.round(val * base) / base;
};

module.exports.toObject = (data, key, val) => {
    if (!Array.isArray(data)) throw new Error("INVALID_DATA");
    if (!key || typeof key !== "string") throw new Error("INVALID_KEY");

    const newObj = {};
    if (data.length > 0) {
        for (const item of data) {
            newObj[String(item[key])] = !!val ? item[val] : item;
        }
    }
    return newObj;
};
