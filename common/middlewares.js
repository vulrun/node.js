const _ = require("lodash");

module.exports.logger = () => (req, res, next) => {
    // Route Logger
    console.log("Express", req.method, res.statusCode, req.originalUrl || req.url);

    // proceed next
    next();
};

module.exports.responses = () => (req, res, next) => {
    // success response
    res.success = (message, data) => {
        if (/^[A-Z_]+$/.test(message)) {
            message = _.lowerCase(message);
            message = _.upperFirst(message);
        }
        return res.send({ status: 200, message, data: data || {} });
    };

    // error response
    res.error = (msg, code, data) => {
        if (/^[A-Z_]+$/.test(msg)) {
            msg = _.lowerCase(msg);
            msg = _.upperFirst(msg);
        }
        res.status(400).send({ status: code || 400, message: msg, data: data || {} });
    };

    // proceed next
    next();
};

module.exports.all = () => [this.logger(), this.responses()];
