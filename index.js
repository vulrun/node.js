"use strict";
require("dotenv").config();

const express = require("express");
const app = express();
const cors = require("cors");
const { connection, middlewares } = require("./common");

// Listening
app.listen(process.env.PORT, () => {
    console.log("Listening on", process.env.PORT);
    connection.mongodb();
});

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/static", express.static(__dirname + "/public"));
app.use(middlewares.all());

// routes
const v1Routes = require("./v1/routes");
app.use("/v1", v1Routes);

// 404
app.use((req, res, next) => res.error("404, Not Found", 404));

// Errors
app.use((err, req, res, next) => {
    console.error(err);
    return res.error(err.message, 400);
});
