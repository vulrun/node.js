const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema.Types;

const SessionSchema = new mongoose.Schema(
    {
        userId: { type: ObjectId, ref: "Users" },
        ipAddress: { type: String, default: "" },
        userAgent: { type: String, default: "" },
        accessToken: { type: String, default: "" },
        isDeleted: { type: Boolean, default: false },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Sessions", SessionSchema);
