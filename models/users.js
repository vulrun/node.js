const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
    {
        username: { type: String, default: "" },
        password: { type: String, default: "" },
        fullName: { type: String, default: "" },
        isBlocked: { type: Boolean, default: false },
        isDeleted: { type: Boolean, default: false },
        isVerified: { type: Boolean, default: false },
        resetToken: { type: String, default: "" },
    },
    { timestamps: true }
);

UserSchema.index({ username: 1 }, { unique: true, dropDups: true });

UserSchema.methods.authenticate = function (password, callback) {
    const promise = new Promise((resolve, reject) => {
        if (!password) reject(new Error("Missing Password"));

        bcrypt.compare(password, this.password, (error, result) => {
            if (!result) reject(new Error("Wrong Password"));
            resolve(this);
        });
    });

    if (typeof callback != "function") return promise;
    promise.then((result) => callback(null, result)).catch((err) => callback(err));
};

UserSchema.methods.setPassword = function (password, callback) {
    const promise = new Promise((resolve, reject) => {
        if (!password) reject(new Error("Missing Password"));

        bcrypt.hash(password, 10, (err, hash) => {
            if (err) reject(err);
            this.password = hash;
            resolve(this);
        });
    });

    if (typeof callback != "function") return promise;
    promise.then((result) => callback(null, result)).catch((err) => callback(err));
};

module.exports = mongoose.model("users", UserSchema);
