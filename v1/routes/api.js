const router = require("express").Router();
const Controller = require("../controllers");
const Auth = require("../../common/authenticate");

router.post("/register", Controller.api.register);
router.post("/login", Controller.api.login);
router.post("/logout", Controller.api.logout);
router.post("/sendOtp", Auth.verifyUser, Controller.api.sendOtp);
router.post("/verifyOtp", Auth.verifyUser, Controller.api.verifyOtp);
router.get("/getProfile", Auth.verifyUser, Controller.api.getProfile);
router.put("/updateProfile", Auth.verifyUser, Controller.api.updateProfile);
router.put("/changePassword", Auth.verifyUser, Controller.api.changePassword);
router.put("/resetPassword", Auth.verifyUser, Controller.api.resetPassword);
router.delete("/deleteAccount", Auth.verifyUser, Controller.api.deleteAccount);

module.exports = router;
